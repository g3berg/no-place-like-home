// parameters
let p = {
  boolean: true,
  numeric: 50,
  numericMin: 0,
  numericMax: 100,
  numericStep: 1,
};

let bed;
let books;
let fridge;
let island;
let capture;
let toilet;
let rooms;
let callibrate;
let bunkbed;
let piano;
let television;
let frame;
let lastRoom; 
let pianoNoise;
let bedTextures;
let bookTextures;
let sinkTextures;
let dresser;
let dresserTextures;
let pianoBench;
let couch;
let couchTextures;
let coffeeTable;
let tableTextures;
let hardwoodTexture;
let laminateTexture;
let static;
let bathroomWalls;
let bathroomFloorTexture;
let livingroomWalls;
let pianoRoomWalls;
let playroomWalls;

let momWall;
let checks;
let n;
let bedroomNoises;
let bedroomWalls;
let bunkbedTextures;
let fridgeTextures;
let sink;
let islandTextures;
let kitchenNoises;
let pianoTextures;
let toiletTextures;
let bathroomNoises;
let televisionTextures;
let kitchenWalls;
let drywallTexture;
let mombed;
let mombedTextures;
let deskTextures;
let cabinets;
let cabinetsTextures;
let desk; 
let primaryRoom;
let secondaryRoom;
let strengths;
let mostStrong;

class Furniture {
  constructor(model, textures, dimensions, scale, x, y, z) {
    this.model = model;
    this.textures = textures;
    this.dimensions = dimensions;
    this.scale = scale;
    this.x = x;
    this.y = y;
    this.z = z;
  }
}

class Room {
  constructor(name, furniture, sounds, walls, floor, tint) {
    this.name = name;
    this.intensity = 0; 
    this.furniture = furniture;
    this.sounds = sounds;
    this.walls = walls;
    this.floor = floor;
    this.tint = tint; 
  }
}


function preload() {
  strengths = [0, 0, 0, 0, 0, 0, 0];
  checks = [[635.5, 132], [310.5, 144], [735.5, 383], [442.5, 421], [227.5, 466],  [743.5, 736], [192.5, 806]];
  font = loadFont('linear_beam/Linebeam.ttf');

  //bedroom 
  bed = loadModel('objects/bed2.obj', true);
  bedTextures = [loadImage('objects/bedIkea.avif'), loadImage('objects/bedTexture1.jpeg')];
  
  pianoNoise = [loadSound('objects/piano.wav')];
  books = loadModel('objects/books2.obj', true);
  bookTextures = [loadImage('objects/bookshelfTexture.jpeg'), loadImage('objects/bookTexture2.jpeg'), loadImage('objects/bookTexture3.jpeg')];

  desk = loadModel('objects/desk.obj', true);
  deskTextures = [loadImage('objects/deskTexture1.jpeg'), loadImage('objects/deskTexture2.jpeg')]

  bedroomNoises = [loadSound('objects/bedroomNoise1.mp3')];
  static = loadSound('objects/static.mp3');
  bedroomWalls = [loadImage('objects/bedroomWall.jpg'),loadImage('objects/bedroomWall.jpg'), loadImage('objects/bedroomWall.jpg')];

  //playroom
  bunkbed = loadModel('objects/bunkBed.obj', true);
  bunkbedTextures = [loadImage('objects/bunkbedTexture1.jpeg'), loadImage('objects/bunkbedTexture2.jpeg')];
  dresser = loadModel('objects/dresser.obj', true);
  dresserTextures = [loadImage('objects/dresserTexture1.jpeg'), loadImage('objects/dresserTexture2.jpeg')];
  playroomWalls =  [loadImage('objects/playroomWall1.jpeg'), loadImage('objects/playroomWall2.jpeg'), loadImage('objects/playroomWall3.jpeg')];

  //kitchen
  fridge = loadModel('objects/fridge2.obj', true);
  fridgeTextures = [loadImage('objects/fridgeTexture1.jpeg'), loadImage('objects/fridgeTexture2.jpeg')];
 
  cabinets = loadModel('objects/cabinets.obj', true);
  cabinetsTextures = [loadImage('objects/cabinet copy.jpg'),loadImage('objects/cabinetTexture1.jpg')];
  island = loadModel('objects/island2.obj', true);
  islandTextures = [loadImage('objects/islandTexture1.jpeg'), loadImage('objects/islandTexture2.jpeg'), loadImage('objects/islandTexture3.jpeg')];

  kitchenWalls = [loadImage('objects/kitchenWall.jpeg'), loadImage('objects/kitchenWall2.jpeg'), loadImage('objects/tiles.jpeg') ];
  kitchenNoises = [loadSound('objects/kitchenNoise1.mp3'), loadSound('objects/kitchenNoise2.mp3'), loadSound('objects/kitchenNoise3.mp3')];
  n = kitchenNoises[1];
  //piano room
  piano = loadModel('objects/piano.obj', true);
  pianoBench = loadModel('objects/concertStool.obj', true);
  pianoTextures = [loadImage('objects/pianoTexture1.jpeg'), loadImage('objects/pianoTexture2.jpeg'), loadImage('objects/pianoTexture3.jpeg')];
  pianoRoomWalls = [loadImage('objects/greenPaint.jpg'), loadImage('objects/greenPaint.jpg'), loadImage('objects/greenPaint.jpg')];
  //pianoCupboard =  loadModel('objects/cabinets.obj', true);
 // pianoCupboardTextures = dresserTextures;
  //bathroom
  toilet = loadModel('objects/toilet2.obj', true);
  toiletTextures = [loadImage('objects/download.jpeg')];
  bathroomNoises = [loadSound('objects/bathroomNoise1.mp3'), loadSound('objects/bathroomNoise2.mp3')];
  bathroomWalls = [loadImage('objects/bathroomWall1.jpeg'), loadImage('objects/bathroomWall2.jpeg'), loadImage('objects/showerWall.jpg')];

  sink = loadModel('objects/sink.obj', true);
  sinkTextures = [loadImage('objects/sinkTexture1.jpeg'), loadImage('objects/sinkTexture2.jpeg')];

  suburb = [loadSound('objects/suburb.wav')];

  //living room
  television = loadModel('objects/tv.obj', true);
  televisionTextures =  [loadImage('objects/televisionTexture1.jpeg'), loadImage('objects/televisionTexture2.jpeg'), loadImage('objects/televisionTexture4.jpg')];

  coffeeTable = loadModel('objects/coffeeTable.obj', true);
  tableTextures = dresserTextures;

  couch =  loadModel('objects/sofa.obj', true);
  couchTextures = [loadImage('objects/couchTexture1.jpeg'), loadImage('objects/couchTexture2.jpeg'), loadImage('objects/couchTexture3.jpeg')];

  livingroomWalls =  [loadImage('objects/livingroomWall3.jpeg'), loadImage('objects/livingroomWall2.jpeg'), loadImage('objects/livingroomWall1.jpeg')];
  //mom and dad bedroom
  mombed = loadModel("objects/mombed.obj");
  mombedTextures = [loadImage('objects/mombedTexture1.jpeg'), loadImage('objects/mombedTexture2.jpeg'), loadImage('objects/mombedTexture3.jpeg')];
  momWall = [loadImage('objects/wallpaper1.jpeg'), loadImage('objects/wallpaper2.jpeg'), loadImage('objects/wallpaper3.jpeg')];
  livingRoomNoises = [loadSound('objects/computer.wav')];
  kidsSound = [loadSound('objects/kids.wav')];
  callibrate = false; //turn callibration on / off

  drywallTexture = loadImage('objects/drywall.jpeg');
  bathroomFloorTexture = loadImage('objects/bathroomFloorTexture.avif');
  hardwoodTexture = loadImage('objects/hardwoodTexture.jpeg');
  laminateTexture = loadImage('objects/darkLaminateTexture.jpeg');

}

function setup() {
  mostStrong = 0;
  frame = 0;
  angleMode(DEGREES) 
  createCanvas(1000, 1000, WEBGL);
  createParamGui(p, paramChanged);
  _paramGui.setPosition(10, 10); // can customize where GUI is drawn
  select("body").style("background: #000000;");
  capture = createCapture(VIDEO);
  capture.size(1000, 1000);
  //capture.hide();

  //initialize all furniture with: obj, textures, dimensions, scale, direction 
  
  let bedF = new Furniture(bed, bedTextures, [300, 400, -600], 4,  180, 180, 0);
  let sinkF = new Furniture(sink, sinkTextures, [300, 200, -600], 2, 180, 180, 0);
  let bookF = new Furniture(books, bookTextures, [-230, 320, -800], 2, 180, 180, 0);
  let bunkbedF = new Furniture (bunkbed, bunkbedTextures, [350, 400, -600], 2.5,  180, 90, 0);
  let bunkbedF2 = new Furniture (bunkbed, bunkbedTextures, [-350, 400, -600], 2.5,  180, 90, 0);
  let dresserF = new Furniture (dresser, dresserTextures, [0, 200, -600], 2.5,  0, 0, 0);
  let fridgeF = new Furniture (fridge, fridgeTextures, [-330, 200, -800], 2.8,  180, 180, 0);
  let islandF = new Furniture (island, islandTextures, [-50, 380, -100], 2,  180, 180, 0);
  let pianoF = new Furniture (piano, pianoTextures, [-400, 300, -300], 3, 180, 90, 0);//
  let stoolF = new Furniture (pianoBench, pianoTextures, [-300, 470, -300], 1, 90, 0, 0);//
  let toiletF = new Furniture (toilet, toiletTextures, [-230, 320, -600], 3, 90, 0, 90);//
  let televisionF = new Furniture (television, televisionTextures, [370, 300, -600], 5,  180, -90, 0);
  let couchF = new Furniture(couch, couchTextures, [-295, 300, -300], 3, -90, 180, 0);
  let deskF = new Furniture (desk, deskTextures, [-300, 320, -100], 2,180, 90, 0);
  let cabinetF = new Furniture (cabinets, cabinetsTextures, [150, 100, -750], 3.7,  180, 0, 0);
  let cupboardF = new Furniture (cabinets, dresserTextures, [150, 100, -750], 3.7,  180, 0, 0);
  let mombedF = new Furniture (mombed, mombedTextures, [0, 450, -750], 0.4,  180, 180, 0);
  let coffeeTableF = new Furniture (coffeeTable, tableTextures, [0, 450, -400], 1.5,  180, 180, 0);
  /*
  bathroomFloorTexture = loadImage('objects/bathroomFloorTexture.avif');
  hardwoodTexture = loadImage('objects/hardwoodTexture.jpeg');
  laminateTexture = loadImage('objects/darkLaminateTexture.jpeg');

  */
//furniture, sounds, walls, area (x1 y1 x2 y2), 
  let bedroom = new Room("bedroom", [bedF, bookF, deskF], bedroomNoises, bedroomWalls, hardwoodTexture, [100, 100, 250]);
  let kitchen = new Room("kitchen", [fridgeF, islandF, cabinetF], kitchenNoises, kitchenWalls, laminateTexture, [100, 100, 100]);
  let bathroom = new Room("bathroom", [toiletF, sinkF], bathroomNoises, bathroomWalls, bathroomFloorTexture, [100, 100, 100]);
  let livingroom = new Room("livingroom", [televisionF, couchF, coffeeTableF], livingRoomNoises, livingroomWalls, laminateTexture, [100, 100, 100], );
  let pianoroom = new Room("pianoroom", [pianoF, stoolF, cupboardF], pianoNoise, pianoRoomWalls, hardwoodTexture, [200, 255, 200]);
  let playroom = new Room("playroom", [bunkbedF, bunkbedF2, dresserF], kidsSound, playroomWalls, hardwoodTexture, [200, 100, 100]);
  let momanddadroom = new Room("momanddadroom", [mombedF], suburb, momWall, hardwoodTexture, [255, 255, 255]);

  rooms = [bedroom, playroom, kitchen, momanddadroom, pianoroom, livingroom, bathroom];
  primaryRoom = rooms[0];
}

function checkLights(){

  capture.loadPixels(); //load in all pixel data  
//            bedroom       playroom      kitchen       momanddad     pianoroom      livingroom    bathroom


 for(let i = 0; i < rooms.length; i++){
  let c = capture.get(checks[i][0], checks[i][1])
  strengths[i] = c[0] + c[1] + c[2];
  if(strengths[i] > mostStrong || i == 0){
    mostStrong = strengths[i];
    primaryRoom = rooms[i];
   // piano needs more furniture and walls
  }
 }
}

function draw() {
  frame++;
  if (callibrate == false){
    if(frame%10 == 0){checkLights();} 
    if(mostStrong < 500){
      loadPixels();
      noFill();
      static.play();
      for (let x = 0; x < 1000; x++) {
        for (let y = 0; y < 5000; y++) {
          let index = (x+y*1000)*4;
          let r = random(0, 1);
          pixels[index+0]=(r*255);
          pixels[index+1]=(r*255);
          pixels[index+2]=(r*255);
          pixels[index+3]=255;
        }
      }
      updatePixels();
    }
  else{
    static.stop();
    //   mySound.play();
    console.log(primaryRoom.name);
    if(primaryRoom.sounds.length > 0){
      if(frame%10 == 0){
        n = random(primaryRoom.sounds);
        n.play();
        n.stop(5);
      } 
    }
  
    console.log(primaryRoom.name);
  // walls 
    background(240);
    noStroke();
    noFill();

    push();
    texture(primaryRoom.walls[0]);
    translate(0, 0, -1000);
    plane(1000);
    pop();

    push();
    texture(primaryRoom.walls[1]);
    translate(500, 0, -500);
    rotateY(-90);
    plane(1000);
    pop();

    push();
    texture(primaryRoom.walls[2]);
    translate(-500, 0, -500);
    rotateY(90);
    plane(1000);
    pop();
    //texture(tilesTexture);

    push();
    texture(primaryRoom.floor);
    translate(0, 500, -500);
    rotateX(90);
    rotateZ(90);
    plane(1000);
    pop();
  
    fill(10, 10, 10);
    push();
    tint(primaryRoom.tint[0],primaryRoom.tint[1],primaryRoom.tint[2]);
    texture(drywallTexture);
    translate(0, -500, -500);
    rotateX(90);
    rotateZ(90);
    plane(1000);
    pop();

    for(let i = 0; i < primaryRoom.furniture.length; i++){ 
      push();
      let currentFurniture = primaryRoom.furniture[i];
      translate(currentFurniture.dimensions[0], currentFurniture.dimensions[1], currentFurniture.dimensions[2]);
      rotateX(currentFurniture.x);
      rotateY(currentFurniture.y);
      rotateZ(currentFurniture.z);
      noStroke();
      scale(currentFurniture.scale);
      texture(random(currentFurniture.textures));
      model(currentFurniture.model);
      pop(); 
    }

  }

  }
  if(callibrate){

    push();
    image(capture, -500, -500, 1000, 1000);
    pop();

    push();
    ellipse(310.5 - 500, 144 - 500, 10);
    ellipse(635.5 - 500, 132 - 500, 10);
    ellipse(227.5 - 500, 466 - 500, 10);
    ellipse(442.5 - 500, 421 - 500, 10);
    fill(255, 0, 0);
    ellipse(735.5 - 500, 383 - 500, 10);
    fill(0, 255, 0);
    ellipse(192.5 - 500, 806 - 500, 10);
    fill(0, 0, 255);
    ellipse(743.5 - 500, 736 - 500, 10);
    pop();

  }

}

function keyPressed() {
  if (key == " ") {
  }
}

function mousePressed() {
  return false;
}

function windowResized() {}

// global callback from the settings GUI
function paramChanged(name) {}
